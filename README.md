
# Link in Bio
Application in pure html, css and js, to show all your links in one place, implementing the [Vanta.js](https://www.vantajs.com/) library, to have a more interactive effect.

The backgound images are provide by Unsplash.

[Unsplash](https://unsplash.com/) photos are made to be used freely.